package com.example.marketpulsedemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * User: anuj
 * Date: 2/5/19
 * Time: 3:50 PM
 */

abstract class BaseActivity : AppCompatActivity() {


    abstract fun init()
    abstract fun getLayout(): Int


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        init()
    }

}
