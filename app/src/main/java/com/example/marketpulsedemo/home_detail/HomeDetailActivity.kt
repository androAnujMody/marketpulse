package com.example.marketpulsedemo.home_detail

import android.support.v4.content.ContextCompat
import com.example.marketpulsedemo.BaseActivity
import com.example.marketpulsedemo.R
import com.example.marketpulsedemo.home.HomeModel
import com.example.marketpulsedemo.utils.Constants
import kotlinx.android.synthetic.main.activity_home_detail.*

/**
 * User: anuj
 * Date: 3/5/19
 * Time: 1:08 PM
 */
class HomeDetailActivity : BaseActivity() {

    lateinit var homeModel: HomeModel

    override fun getLayout(): Int = R.layout.activity_home_detail

    override fun init() {
        supportActionBar?.title = "Detail"


        homeModel = intent.getParcelableExtra(Constants.MODEL)

        tv_header.text = homeModel.name
        tv_tag.text = homeModel.tag

        when (homeModel.color.toLowerCase()) {
            "green" -> tv_tag.setTextColor(ContextCompat.getColor(this, R.color.green))
            "red" -> tv_tag.setTextColor(ContextCompat.getColor(this, R.color.red))
            else -> tv_tag.setTextColor(
                ContextCompat.getColor(
                    this,
                    android.R.color.darker_gray
                )
            )
        }


        tv_desc.text = when (homeModel.criteria.size) {
            1 -> displayDesc(homeModel.criteria[0].text, 0)
            2 -> {
                "${displayDesc(homeModel.criteria[0].text, 0)}\n" +
                        "and\n\n" +
                        "${displayDesc(homeModel.criteria[1].text, 1)}"
            }
            3 -> {
                "${displayDesc(homeModel.criteria[0].text, 0)}\n" +
                        "and\n\n" +
                        "${displayDesc(homeModel.criteria[1].text, 1)}\n" +
                        "and\n\n" +
                        "${displayDesc(homeModel.criteria[2].text, 2)}"
            }
            4 -> {
                "${displayDesc(homeModel.criteria[0].text, 0)}\n" +
                        "and\n\n" +
                        "${displayDesc(homeModel.criteria[1].text, 1)}\n" +
                        "and\n\n" +
                        "${displayDesc(homeModel.criteria[2].text, 2)}\n" +
                        "and\n\n" +
                        "${displayDesc(homeModel.criteria[3].text, 3)}"
            }
            else -> {
                ""
            }
        }

    }

    private fun displayDesc(text: String, pos: Int): String {


        var finalText = text
        if (homeModel.criteria[pos].variable?.one?.values == null) {
            finalText = finalText.replace("\$1", "${homeModel.criteria[pos].variable?.one?.defaultValue}", true)
        } else {
            finalText = finalText.replace("\$1", "${homeModel.criteria[pos].variable?.one?.values?.get(0)}", true)
        }
        finalText = finalText.replace("\$2", "${homeModel.criteria[pos].variable?.two?.values?.get(0)}", true)
        finalText = finalText.replace("\$3", "${homeModel.criteria[pos].variable?.three?.values?.get(0)}", true)
        finalText = finalText.replace("\$4", "${homeModel.criteria[pos].variable?.four?.defaultValue}", true)

        return finalText
/*
        return when {
            text.contains("$1") -> {

                if (homeModel.criteria[pos].variable != null) {
                    text.replace("$1", "${homeModel.criteria[pos].variable?.one?.values?.get(0)}")
                } else {
                    text
                }


            }
            text.contains("$2") -> {

                if (homeModel.criteria[pos].variable != null) {
                    text.replace("$2", "${homeModel.criteria[pos].variable?.two?.values?.get(0)}")
                } else {
                    text
                }
            }
            text.contains("$3") -> {

                if (homeModel.criteria[pos].variable != null) {
                    text.replace("$3", "${homeModel.criteria[pos].variable?.three?.values?.get(0)}")
                } else {
                    text
                }
            }
            text.contains("$4") -> {
                if (homeModel.criteria[pos].variable != null) {
                    text.replace("$4", "${homeModel.criteria[pos].variable?.four?.defaultValue}")
                } else {
                    text
                }
            }
            else -> {
                text
            }
        }
*/
    }


}
