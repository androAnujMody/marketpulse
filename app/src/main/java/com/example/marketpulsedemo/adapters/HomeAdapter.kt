package com.example.marketpulsedemo.adapters

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.marketpulsedemo.R
import com.example.marketpulsedemo.home.HomeModel
import com.example.marketpulsedemo.home_detail.HomeDetailActivity
import com.example.marketpulsedemo.utils.Constants
import com.example.marketpulsedemo.utils.inflate
import kotlinx.android.synthetic.main.item_home.view.*

/**
 * User: anuj
 * Date: 2/5/19
 * Time: 7:05 PM
 */

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeVH>() {


    private var list: List<HomeModel> = emptyList()


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HomeVH {

        return HomeVH(p0.inflate(R.layout.item_home))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: HomeVH, p1: Int) {

        p0.setData(list[p1])

    }

    class HomeVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var model: HomeModel

        init {

            itemView.setOnClickListener {

                itemView.context.startActivity(
                    Intent(itemView.context, HomeDetailActivity::class.java)
                        .putExtra(Constants.MODEL, model)
                )
            }

        }

        fun setData(homeModel: HomeModel) {

            model = homeModel

            itemView.tv_header.text = homeModel.name
            itemView.tv_tag.text = homeModel.tag

            when (homeModel.color.toLowerCase()) {
                "green" -> itemView.tv_tag.setTextColor(ContextCompat.getColor(itemView.context, R.color.green))
                "red" -> itemView.tv_tag.setTextColor(ContextCompat.getColor(itemView.context, R.color.red))
                else -> itemView.tv_tag.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        android.R.color.darker_gray
                    )
                )
            }

        }


    }

    fun setListData(list: List<HomeModel>?) {
        this.list = list ?: emptyList()
        notifyDataSetChanged()
    }

}
