package com.example.marketpulsedemo.utils

import com.example.marketpulsedemo.home.HomeModel
import retrofit2.Call
import retrofit2.http.GET


/**
 * User: anuj
 * Date: 2/5/19
 * Time: 5:34 PM
 */

interface RetrofitInterface {
    @GET("data")
    fun getData(): Call<List<HomeModel>>
}
