package com.example.marketpulsedemo.utils

import android.content.Context
import android.net.ConnectivityManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

/**
 * User: anuj
 * Date: 2/5/19
 * Time: 3:56 PM
 */


internal fun Context.isNetworkAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo.isConnected
}


internal fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

internal val View.getContext: Context
    get() = context

internal fun ViewGroup.inflate(layoutId: Int): View {
    return LayoutInflater.from(context).inflate(layoutId, this, false)
}

internal fun View.hideIt() {
    this.visibility = View.GONE
}

internal fun View.showIt() {
    this.visibility = View.VISIBLE
}



 
