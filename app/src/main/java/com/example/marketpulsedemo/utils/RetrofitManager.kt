package com.example.marketpulsedemo.utils

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * User: anuj
 * Date: 2/5/19
 * Time: 4:33 PM
 */

class RetrofitManager {


    companion object {

        fun init(): RetrofitInterface {
            val logging = HttpLoggingInterceptor()

            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                    RxJava2CallAdapterFactory.create()
                )
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .client(httpClient.build())
                .baseUrl("https://mp-android-challenge.herokuapp.com/")
                .build()
            return retrofit.create(RetrofitInterface::class.java)
        }
    }
}
