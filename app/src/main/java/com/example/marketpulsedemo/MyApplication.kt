package com.example.marketpulsedemo

import android.app.Application
import com.example.marketpulsedemo.utils.RetrofitInterface
import com.example.marketpulsedemo.utils.RetrofitManager

/**
 * User: anuj
 * Date: 2/5/19
 * Time: 4:40 PM
 */

val retrofitService by lazy {
    MyApplication.retrofitInterface
}

class MyApplication : Application() {


    companion object {
        lateinit var retrofitInterface: RetrofitInterface
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()

        retrofitInterface = RetrofitManager.init()
    }

}
