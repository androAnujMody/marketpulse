package com.example.marketpulsedemo.home

import com.example.marketpulsedemo.retrofitService
import retrofit2.Call

/**
 * User: anuj
 * Date: 2/5/19
 * Time: 4:45 PM
 */

class HomeRepo {


    companion object {
        val instance = HomeRepo()
    }

    fun getData(): Call<List<HomeModel>> {
        return retrofitService.getData()
    }
}
