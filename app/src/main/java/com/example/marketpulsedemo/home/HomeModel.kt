package com.example.marketpulsedemo.home

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * User: anuj
 * Date: 2/5/19
 * Time: 6:40 PM
 */
@Parcelize
data class HomeModel(
    val id: String,
    val name: String,
    val tag: String,
    val color: String,
    val criteria: List<Criterium>
) : Parcelable

@Parcelize
data class Criterium(
    var type: String,
    var text: String,
    var variable: Variable? = null
) : Parcelable


@Parcelize
data class Variable(
    @SerializedName("$1")
    @Expose
    var one: One?,
    @SerializedName("$2")
    @Expose
    var two: Two?,
    @SerializedName("$3")
    @Expose
    var three: Three?,
    @SerializedName("$4")
    @Expose
    var four: Four?
) : Parcelable


@Parcelize
data class One(
    var type: String,
    @SerializedName("values")
    @Expose
    var values: List<Int>?,
    @SerializedName("default_value")
    @Expose
    var defaultValue: Int
) : Parcelable


@Parcelize
data class Two(
    var type: String,
    @SerializedName("values")
    @Expose
    var values: List<Int>?

) : Parcelable

@Parcelize
data class Three(
    var type: String,
    @SerializedName("values")
    @Expose
    var values: List<Double>?

) : Parcelable


@Parcelize
data class Four(
    var type: String,
    @SerializedName("values")
    @Expose
    var values: List<Int>?,
    @SerializedName("default_value")
    @Expose
    var defaultValue: Int
) : Parcelable


