package com.example.marketpulsedemo.home

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import com.example.marketpulsedemo.BaseActivity
import com.example.marketpulsedemo.R
import com.example.marketpulsedemo.adapters.HomeAdapter
import com.example.marketpulsedemo.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : BaseActivity() {

    private val presenter by lazy {
        HomePresenter()
    }

    private val adapter by lazy {
        HomeAdapter()
    }

    override fun getLayout(): Int = R.layout.activity_main

    override fun init() {

        supportActionBar?.title = "Home"
        rv_list.adapter = adapter
        rv_list.itemAnimator = DefaultItemAnimator()
        rv_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        if (isNetworkAvailable()) {
            getDataFromServer()
        } else {
            showToast(getString(R.string.no_internet))
        }


    }

    private fun getDataFromServer() {

        presenter.getData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { t: Resource<List<HomeModel>>? ->
                when (t?.status) {

                    Status.SUCCESS -> {
                        progress.hideIt()
                        adapter.setListData(t.data)
                    }

                    Status.LOADING -> {
                        progress.showIt()
                    }

                    Status.ERROR -> {
                        t.message?.let { showToast(it) }
                        progress.hideIt()
                    }
                }
            }

    }
}


