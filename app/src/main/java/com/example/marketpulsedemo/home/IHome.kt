package com.example.marketpulsedemo.home

import com.example.marketpulsedemo.utils.Resource
import io.reactivex.subjects.PublishSubject

/**
 * User: anuj
 * Date: 2/5/19
 * Time: 4:04 PM
 */

interface IHome {

    interface View;

    interface Actions {

        fun getData(): PublishSubject<Resource<List<HomeModel>>>
    }
}
