package com.example.marketpulsedemo.home

import com.example.marketpulsedemo.utils.Resource
import io.reactivex.subjects.PublishSubject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * User: anuj
 * Date: 2/5/19
 * Time: 4:06 PM
 */

class HomePresenter : IHome.Actions {

    private val homeRepo by lazy {
        HomeRepo.instance
    }

    private val data: PublishSubject<Resource<List<HomeModel>>> = PublishSubject.create()

    override fun getData(): PublishSubject<Resource<List<HomeModel>>> {

        data.onNext(Resource.loading())
        homeRepo.getData().enqueue(object : Callback<List<HomeModel>?> {
            override fun onFailure(call: Call<List<HomeModel>?>, t: Throwable) {
                data.onNext(Resource.error(t.localizedMessage, null))
            }

            override fun onResponse(call: Call<List<HomeModel>?>, response: Response<List<HomeModel>?>) {
                data.onNext(Resource.success(response.body()))
            }
        })

        return data

    }

}
